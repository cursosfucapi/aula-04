﻿using System;

namespace loopando
{
    class Program
    {
        static void Main(string[] args)
        {
            int qtde = 0, idade = 0;

            for (int a = 1; a <= 10; a++){
                Console.WriteLine("Informe a idade: ");
                idade = Int32.Parse(Console.ReadLine());

                if(idade >= 18){
                    qtde++;
                }
            }
            Console.WriteLine("Qtde. de pessoas maiores de 18 anos: {0}",qtde);

            //int contador = 0;
            //for (int i = 2; i < 10; i++)
            //{
            //    Console.WriteLine("Tabuada de {0}",i);
            //    for (int p = 1; p < 10; p++)
            //    {
            //        Console.WriteLine("{0} X {1} = {2}", i, p,(i * p));
            //        contador++;
            //    }
            //    Console.WriteLine("\n");

            //}
            //Console.WriteLine("Rodou muitas vezes, rodou {0}",contador);
        }
    }
}
